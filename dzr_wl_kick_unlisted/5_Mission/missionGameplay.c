modded class MissionGameplay
{

	protected Widget m_DZRIDz_root;	
	protected TextWidget m_dzrDialogTitle;
	protected RichTextWidget m_dzr_dialogText;
	bool isErrorNew = false;
	
	void MissionGameplay()
	{
		Print("[KICKS][Client] ::: MissionGameplay ::: Initiated");
		GetRPCManager().AddRPC( "DZR_IDZ_RPC_TOclient", "DZR_MissionGameplay_SendPlayerMessage", this, SingleplayerExecutionType.Both );
	}
	
	void DZR_MissionGameplay_SendPlayerMessage(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		
		Print("[KICKS][Client] ::: Got DZR_IDZ_RPC_TOclient ::: DZR_MissionGameplay_SendPlayerMessage");
		Param1<string> data;
		if ( !ctx.Read( data ) ) return;
		
		if (type == CallType.Client)
		{	
			string serverMessage = data.param1;
			Print("[KICKS][Client] ::: Got serverMessage :::"+serverMessage);
			
			GetGame().Chat("||| Got serverMessage RPC from server: "+serverMessage, "colorImportant");
			GetGame().DisconnectSessionForce();
			
			//GetGame().GetUIManager().ShowDialog( "#str_xbox_join_fail_title", "#str_xbox_join_fail", 444, DBT_OK, DBB_NONE, DMT_INFO, GetGame().GetUIManager().GetMenu() );
			/*
			Print("Activating UI");
			m_DZRIDz_root = GetGame().GetWorkspace().CreateWidgets( "dzr_wl_kick_unlisted/GUI/layouts/custom_kick.layout" );
			m_dzrDialogTitle = TextWidget.Cast( m_DZRIDz_root.FindAnyWidget("Caption") );
			m_dzr_dialogText        = RichTextWidget.Cast( m_DZRIDz_root.FindAnyWidget( "Text" ) );
			m_DZRIDz_root.Show(false);
			//m_dzrDialogTitle.Show(true);
			//m_dzr_dialogText.Show(true);
			m_dzrDialogTitle.SetText("KICKING YOU!");
			m_dzr_dialogText.SetText("FOR THE HORDE!");
			*/
// TODO: Server sends RPC, disconnects player. Player gets to modded intro\menu, reads proflie - DZR\whitelist_error\kick.txt
			
			GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.ThrowError, 1000, true);
		}
	}
	void ThrowError()
	{
		if(isErrorNew)
		{
			if ( GetGame().GetMissionState() == DayZGame.MISSION_STATE_MAINMENU )
			{
				isErrorNew = false;
				ErrorModuleHandler.ThrowError(ErrorCategory.ClientKicked, EClientKicked.NOT_WHITELISTED, "Регистрация на DayZRussia.com");
			};
		};
	}

}		

