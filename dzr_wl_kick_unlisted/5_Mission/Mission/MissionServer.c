modded class MissionServer
{
	/*
	ref dzr_idz_config_data_class m_defaultConfig;
	ref dzr_idz_config_data_class m_existingConfig;
	const static string dzr_Idz_ProfileFolder = "$profile:\\";
	const static string dzr_Idz_TagFolder = "DZR\\";
	const static string dzr_Idz_ModFolder = "IdentityZ\\";
    const static string dzr_Idz_ConfigFile = "dzr_idz_config.json";
	*/
	PlayerBase g_thePlayer;
    void MissionServer()
    {
		//TODO: Load config
		Print("[KICKS][Server] ::: MissionServer ::: Initiated");
//		GetRPCManager().AddRPC( "DZR_IDZ_RPC", "DZR_MissionServer_GetDataFromServer", this, SingleplayerExecutionType.Both );
	//	GetRPCManager().AddRPC( "DZR_IDZ_RPC", "DZR_MissionServer_ToggleShowToAll", this, SingleplayerExecutionType.Both );
		
	}
	
    override void InvokeOnConnect(PlayerBase player, PlayerIdentity identity)
    {
        super.InvokeOnConnect(player, identity);	
		g_thePlayer = 	player;
       // player.SavePlayerName(identity.GetName());
        ////Print("[DZR IdentityZ] ::: MissionServer InvokeOnConnect ::: Player name stored to CharacterName: "+identity.GetName());
		//auto newIdentityZ = new Param2<PlayerBase, string>(player, identity.GetName());
		//GetGame().RPCSingleParam(player, DZRIDZRPC.SAVEPLAYERNAME, newIdentityZ, true);
		//Print("[DZR IdentityZ] ::: MissionServer ::: InvokeOnConnect");
		GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.kickPlayerWithNotification, 8000, false);
		
		//Print("[DZR IdentityZ] ::: MissionServer ::: InvokeOnConnect DZR_MissionGameplay_SaveConfigOnClient"+m_Data);
		
		
	};
	
	
	void kickPlayerWithNotification()
	{
		ref Param1<string> m_Data = new Param1<string>("YO, "+g_thePlayer.GetIdentity().GetName()+"! IT'S SERVER!");
		//Print("[DZR IdentityZ] ::: MissionServer ::: InvokeOnConnect ReadServerConfig "+m_Data);
		
		//KickPlayer(g_thePlayer);
		GetRPCManager().SendRPC( "DZR_IDZ_RPC_TOclient", "DZR_MissionGameplay_SendPlayerMessage", m_Data, true, g_thePlayer.GetIdentity());
	};
	
	void KickPlayer(PlayerBase player)
	{
	if (player.GetIdentity())
	{
		PlayerIdentity identity	=	player.GetIdentity();
		string PlayerUID		=	identity.GetPlainId();
		string PlayerNAME		=	identity.GetName();
			ErrorModuleHandler.ThrowError(ErrorCategory.ClientKicked, 123, "test");
		
		
		
		if (player)
		{
			GetGame().SendLogoutTime(player, 15);
			
			if (GetHive())
			{
				// save player
				player.Save();
				// unlock player in DB	
				GetHive().CharacterExit(player);		
			}
			
			player.ReleaseNetworkControls();
			
			if (player.IsAlive() && !player.IsRestrained() && !player.IsUnconscious())
			{
				// remove the body
				player.Delete();	
			}
			else if (player.IsUnconscious() || player.IsRestrained())
			{
				// kill character
				player.SetHealth("", "", 0.0);
			}
		}
		
		// remove player from server
		ErrorModuleHandler.ThrowError(ErrorCategory.ClientKicked, EClientKicked.NOT_WHITELISTED, "Регистрация на DayZRussia.com");
		GetGame().DisconnectPlayer(identity);
		
		Print("::: [Server_WhiteList_Class] ::: KickPlayer() ::: : player: " + PlayerNAME + ", UID: " + PlayerUID);
	}
	else
	{
		Print("::: [Server_WhiteList_Class] ::: KickPlayer() ::: Kick error: PlayerIdentity NULL!");
	}
}
	
	
}



