class CfgPatches
{
	class dzr_modName
	{
		requiredAddons[] = {};
	};
};

class CfgMods
{
	class dzr_modName
	{
		type = "mod";
		author = "DayZRussia.com";
		dir = "dzr_modName";
		name = "dzr_modName";
		dependencies[] = {"Game","World","Mission"};
		class defs
		{
			class gameScriptModule
			{
				files[] = {"dzr_modName/3_Game"};
			};
			class worldScriptModule
			{
				files[] = {"dzr_modName/4_World"};
			};
			class missionScriptModule
			{
				files[] = {"dzr_modName/5_Mission"};
			};
		};
	};
};